from json import JSONEncoder
from datetime import datetime




class DateEncoder(JSONEncoder):
    def default(self, o):
        # if o is an instance of datetime
        if isinstance(o, datetime):
        #    return o.isoformat()
            return o.isoformat()
        # otherwise
        else:
        #    return super().default(o)
            return super().default(o)


class ModelEncoder(DateEncoder, JSONEncoder):
    def default(self, o):
        #   if the object to decode is the same class as what's in the
        if isinstance(o, self.model):
        #   model property, then
        #     * create an empty dictionary that will hold the property names
            d = {}
        #       as keys and the property values as values
        #     * for each name in the properties list
            for property in self.properties:
        #         * get the value of that property from the model instance
                value = getattr(o, property)
        #           given just the property name
        #         * put it into the dictionary with that property name as
                d[property] = value
        #           the key
        #     * return the dictionary
            return d
        #   otherwise,
        else:

        #       return super().default(o)  # From the documentation
            return super().default(o)
